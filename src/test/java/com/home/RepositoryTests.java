package com.home;

import com.home.model.SavedTweet;
import com.home.model.Word;
import com.home.repository.TweetRepository;
import com.home.repository.WordRepository;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by salae on 2016-10-08.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTests {

    @Autowired
    TweetRepository tweetRepository;

    @Autowired
    WordRepository wordRepository;

    private static final String tag = "tag1";

    @Test
    public void createNewTweet() {

        SavedTweet tweet = tweetRepository.save(createTweet());

        Iterable<SavedTweet> result = tweetRepository.findAll();
        assertThat(result, is(Matchers.<SavedTweet>iterableWithSize(1)));

        Iterable<SavedTweet> find = tweetRepository.findByTextContaining("tag1");
        assertThat(find, is(Matchers.<SavedTweet>iterableWithSize(1)));

    }

    @Test
    public void createNewWord() {

        Word word = wordRepository.save(createWord());

        Iterable<Word> words = wordRepository.findAll();
        assertThat(words, is(Matchers.<Word>iterableWithSize(1)));


    }


    public static SavedTweet createTweet() {
        List<String>  hashTags = new ArrayList<>();
        hashTags.add("tag1");
        return new SavedTweet(new Long(1), new Date(),
                new Integer(2),
                "This is a tweet with #tag1",
                hashTags);

    }

    public static Word createWord(){
        return new Word(tag, new Long(1));
    }
}

