var app = angular.module('app', [
    'angular-jqcloud'
]);

app.controller('controller', function($scope, $http, $log) {
    $scope.words = [];

    $http({
        method: 'GET',
        url: './words'
    }).
    success(function (words) {
        words.forEach(function(word){
            $scope.words.push({
                "text": word.text,
                "weight": word.weight,
                "handlers": {
                    click: function () {
                        $scope.getSelected(word.text);
                    }
                }
            });
        });
    }).
    error(function (data, status) {
        $log.error(status);
    });

    $scope.getSelected = function(word) {
        $http({
            method: 'GET',
            url: './savedTweets/search/bytag',
            params:{
                tag:word
            }
        }).
        success(function (data) {
            $scope.tweets=data._embedded.savedTweets;
        }).
        error(function (data, status) {
            $log.error(status);
        });
    }

});