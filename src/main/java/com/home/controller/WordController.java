package com.home.controller;

import com.home.model.Word;
import com.home.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by salae on 2016-10-08.
 */

@Controller
public class WordController {

    @Autowired
    WordService wordService;

    @RequestMapping(value = "/words", method = RequestMethod.GET)
    @ResponseBody
    public List<Word> words(){
        return wordService.words();
    }

}
