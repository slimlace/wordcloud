package com.home.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by salae on 2016-10-08.
 */

@Entity
public class SavedTweet {

    @Id
    Long id;

    @Temporal(value= TemporalType.TIMESTAMP)
    private Date createdAt;

    private Integer retweetCount;

    private String text;

    @ElementCollection(fetch = FetchType.EAGER)
    List<String> hashtags = new ArrayList<>();

    public SavedTweet(Long id, Date createdAt, Integer retweetCount, String text, List<String> hashtags) {
        this.id=id;
        this.createdAt = createdAt;
        this.retweetCount = retweetCount;
        this.text = text;
        getHashtags().addAll(hashtags);

    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Integer getRetweetCount() {
        return retweetCount;
    }

    public String getText() {
        return text;
    }

    public List<String> getHashtags() {
        return hashtags;
    }

    public SavedTweet(){}

}
