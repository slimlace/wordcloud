package com.home.service;

import com.home.model.SavedTweet;
import com.home.model.Word;
import com.home.repository.TweetRepository;
import com.home.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by salae on 2016-10-08.
 */

@Service
public class WordService {

    @Autowired
    WordRepository wordRepository;

    @Autowired
    TweetRepository tweetRepository;

    public void setWordRepository(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    public void setTweetRepository(TweetRepository tweetRepository) {
        this.tweetRepository = tweetRepository;
    }

    //public List<Word> getWordFrequency(){
    @Scheduled(fixedDelay = 10000, initialDelay = 10000)
    public void setWordFrequency(){
        Stream<SavedTweet> tweetStream = StreamSupport.stream(tweetRepository.findAll().spliterator(),false);

        List<String> hashList = tweetStream
                .map(tweet -> tweet.getHashtags())
                .reduce( new ArrayList<String>(),(list1, list2) -> {
                    list1.addAll(list2);
                    return list1;
                });

        List<Word> words = hashList.stream()
                .collect(
                        Collectors.groupingBy(tag -> tag, Collectors.counting()))
                .entrySet()
                    .stream()
                    .sorted(((e1, e2) -> e2.getValue().compareTo(e1.getValue())))
                    .limit(100)
                .map( (entry) -> new Word(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        wordRepository.save(words);
    }

    public List<Word> words (){
         return StreamSupport.stream(wordRepository.findAll().spliterator(), false)
                 .sorted(Comparator.comparing(Word::getWeight))
                .collect(Collectors.toList());


    }
}
